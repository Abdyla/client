﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class FormMatch : Form
    {
        private string playerName;
        ClientController clientController;
        public FormMatch(string playerName)
        {
            InitializeComponent();
            this.playerName = playerName;
            clientController = new ClientController();
        }

        private void FindTeamButton_Click(object sender, EventArgs e)
        {
            clientController.FindTeam(playerName);
            label1.Text = "Противник: идет поиск";
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            string answer = clientController.GetFindStatus(playerName);
            if (answer != "") label1.Text = "Противник: " + answer;
        }

        private void ResultButton_Click(object sender, EventArgs e)
        {
            string answer = clientController.GetResult(playerName);
            if (answer == "закончен") label1.Text = "Противник: ";
        }
    }
}
