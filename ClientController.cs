﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class ClientController
    {
        private string playerName;

        public ClientController(string playerName)
        {
            this.playerName = playerName;
        }

        //Tournament
        public void CreateTournament(string tournamentName, string playerName)
        {
            //создание нового турнира -> добавление команды игрока в турнир
            //Должна быть проверка на то есть ли такое имя турнира -> ответ(успех/ошибка)
        }
        public string[] GetTournamentList()
        {
            //получить список турниров, которые еще не начились (названия турниров)
            return null;
        }
        public void ChooseTournament(string tournamentName, string playerName)
        {
            //выбор турнира из списка -> добавление команды игрока в турнир
            //должен быть ответ на случай, если турнир уже начался(набралось достаточное количество команд)

        }

        //Auth
        public string RegisterPlayer(string playerName, string password)
        {
            //регистрация нового игрока, должна быть проверка на занятость ника. "" - ошибка "nickname" - имя игрока.
            return "";
        }
        public string LogIn(string playerName, string password)
        {
            //вход игрока в систему, проверка пароля
            return "";
        }

        //Match
        public void FindTeam(string playerName)
        {
            //ищет команду противника -> создает матч -> добавляет в список матчей
        }
        public string GetFindStatus(string playerName)
        {
            return ""; //когда найдет команду противника передать ее имя
        }
        public string GetResult(string playerName)
        {
            return ""; //ответ на получение результата матча
        }

        //Team
        public string AddPlayer(string playerName, string playerToAdd)
        {
            return ""; //ответ на добавление в команду(проверка на то сколько в команде челов, проверка на то есть ли игрок такой в системе)
        }
        public void DeletePlayer(string playerName, string playerToDelete)
        {
            //удаляет игрока из команды
        }
    }
}
